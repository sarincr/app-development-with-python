from tkinter import *


root= Tk()
root.title("grid() method")
root.geometry("200x200")


button0= Button(root, text="Acc Number", height=5, width=10)
button0.grid(row=0,column=0)

button0= Button(root, text="Balance", height=5, width=10)
button0.grid(row=0,column=1)

button0= Button(root, text="ID", height=5, width=10)
button0.grid(row=1,column=0)

button0= Button(root, text="Forgot Pin", height=5, width=10)
button0.grid(row=1,column=1)



root.mainloop()