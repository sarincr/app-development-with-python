import tkinter as tk
from docx2pdf import convert
import tkinter.ttk as ttk
from tkinter.messagebox import showinfo
from tkinter.filedialog import askopenfile
import PyPDF2 

root = tk.Tk()


def openfile():
    pdfFileObj = open('example.pdf', 'rb') 
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    print(pdfReader.numPages)
    pageObj = pdfReader.getPage(0)
    print(pageObj.extractText()) 
    showinfo("Done","File Succesfully Extracted")
    pdfFileObj.close() 

label = tk.Label(root, text= "Choose File:- ")
label.grid(row=0, column=0)

button = ttk.Button(root, text="Convert", width=30, command=openfile)
button.grid(row=0, column=1)

root.mainloop()
