import tkinter as tk
from docx2pdf import convert
import tkinter.ttk as ttk
from tkinter.messagebox import showinfo
from tkinter.filedialog import askopenfile


root = tk.Tk()


def openfile():
    X = askopenfile(filetypes = [('Word Files', '*.docx')])
    convert(X.name,'/Y.pdf')
    showinfo("Done","File Succesfully Converted")

label = tk.Label(root, text= "Choose File:- ")
label.grid(row=0, column=0)

button = ttk.Button(root, text="Select", width=30, command=openfile)
button.grid(row=0, column=1)

root.mainloop()